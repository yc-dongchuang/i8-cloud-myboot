@echo off
set APP_NAME=${project.build.finalName}.jar
set JAVA=java
%JAVA% -Xms512m -Xmx512m -Dloader.path=../lib -Dspring.config.location=../conf/ -jar ./%APP_NAME%
pause