package cn.ii8080.i8.gateway.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.ConfigurableApplicationContext;

@EnableDiscoveryClient
@SpringBootApplication
public class I8CloudGatewayServerApplication {
    private static ConfigurableApplicationContext ctx;

    public static void main(String[] args) {
        ctx = SpringApplication.run(I8CloudGatewayServerApplication.class, args);
    }

    public static ConfigurableApplicationContext getCtx() {
        return ctx;
    }
}
