package cn.ii8080.i8.gateway.server.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

@Component
public class ConverterApplicationRunner implements ApplicationRunner {
    /**
     * token校验
     */
    @Override
    public void run(ApplicationArguments args) throws Exception {
        System.out.println("启动完成!");
    }
}
