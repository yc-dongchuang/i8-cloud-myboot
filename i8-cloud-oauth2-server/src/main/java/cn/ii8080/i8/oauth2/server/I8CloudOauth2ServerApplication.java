package cn.ii8080.i8.oauth2.server;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
@MapperScan("cn.ii8080.i8.oauth2.server.mapper")
@EnableDiscoveryClient
@SpringBootApplication
public class I8CloudOauth2ServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(I8CloudOauth2ServerApplication.class, args);
    }

}
