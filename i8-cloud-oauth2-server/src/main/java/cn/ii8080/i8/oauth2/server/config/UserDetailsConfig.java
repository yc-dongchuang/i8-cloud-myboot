package cn.ii8080.i8.oauth2.server.config;

import cn.ii8080.i8.oauth2.server.entity.SysPermission;
import cn.ii8080.i8.oauth2.server.entity.SysUser;
import cn.ii8080.i8.oauth2.server.mapper.SysPermissionMapper;
import cn.ii8080.i8.oauth2.server.service.ISysPermissionService;
import cn.ii8080.i8.oauth2.server.service.ISysUserService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.google.common.collect.Lists;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserDetailsConfig implements UserDetailsService {
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private ISysUserService userService;
    @Autowired
    private ISysPermissionService permissionService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        SysUser userWhere = new SysUser();
        userWhere.setUsername(username);
        SysUser user = userService.getOne(new QueryWrapper<>(userWhere));
        if (user == null) {
            return null;

        }
        // 2. 查询该用户有哪一些权限
        List<SysPermission> sysPermissions =
                permissionService.getGrantedAuthorityByUsername(user.getId());
        List<GrantedAuthority> authorities = Lists.newArrayList();
        for (SysPermission sp : sysPermissions) {
            String url=sp.getUrl();
            String code=sp.getCode();
            //权限标识
            if(StringUtils.isNotEmpty(code)){
                authorities.add(new SimpleGrantedAuthority(code));
            }
        }
//        findSysPermission(user);
        User user1 = new User(user.getUsername(),user.getPassword(),authorities);

        return user1;
//        return new User(username, password, AuthorityUtils.commaSeparatedStringToAuthorityList("test"));
    }

    /**
     * 查询认真信息
     *
     * @param sysUser
     * @throws UsernameNotFoundException
     */
    public void findSysPermission(SysUser sysUser) throws UsernameNotFoundException {
        if (sysUser == null) {
            throw new UsernameNotFoundException("未查询到有效用户信息");
        }

        // 2. 查询该用户有哪一些权限
        List<SysPermission> sysPermissions =
                permissionService.getGrantedAuthorityByUsername(sysUser.getId());

        // 无权限
        if (CollectionUtils.isEmpty(sysPermissions)) {
            return;
        }

        // 存入权限,认证通过后用于渲染左侧菜单
        sysUser.setPermissions(sysPermissions);

        // 3. 封装用户信息和权限信息
        List<GrantedAuthority> authorities = Lists.newArrayList();
        for (SysPermission sp : sysPermissions) {
            //权限标识
            authorities.add(new SimpleGrantedAuthority(sp.getCode()));
        }
        sysUser.setAuthorities(authorities);
    }
}
