package cn.ii8080.i8.oauth2.server.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import org.springframework.beans.BeanUtils;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

/**
 * @author Liu Dong Cai
 * @since 2020-05-05
 */
@Data
@TableName("sys_permission")
public class SysPermission extends Model<SysPermission> {
    public SysPermission() {
    }

    public SysPermission(Object o) {
        BeanUtils.copyProperties(o, this);
    }

    @TableId(value = "id", type = IdType.ASSIGN_UUID)
    private String id;

    @Override
    protected Serializable pkVal() {
        return id;
    }

    @TableField("name")
    private String name;

    @TableField("code")
    private String code;

    @TableField("url")
    private String url;

    @TableField("type")
    private Integer type;

    @TableField("create_date")
    private Date createDate;

    @TableField("update_date")
    private Date updateDate;

    /**
     * 所有子权限对象集合
     * 左侧菜单渲染时要用
     */
    @TableField(exist = false)
    private List<SysPermission> children;

    /**
     * 所有子权限 URL 集合
     * 左侧菜单渲染时要用
     */
    @TableField(exist = false)
    private List<String> childrenUrl;

}
