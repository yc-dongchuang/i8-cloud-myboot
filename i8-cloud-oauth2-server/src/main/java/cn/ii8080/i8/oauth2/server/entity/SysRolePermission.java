package cn.ii8080.i8.oauth2.server.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import org.springframework.beans.BeanUtils;

import java.io.Serializable;

/**
 * @author Liu Dong Cai
 * @since 2020-05-05
 */
@Data
@TableName("sys_role_permission")
public class SysRolePermission extends Model<SysRolePermission> {
    public SysRolePermission() {
    }

    public SysRolePermission(Object o) {
        BeanUtils.copyProperties(o, this);
    }

    @TableId(value = "id", type = IdType.ASSIGN_UUID)
    private String id;

    @Override
    protected Serializable pkVal() {
        return id;
    }

    @TableField("role_id")
    private String roleId;

    @TableField("permission_id")
    private String permissionId;

}
