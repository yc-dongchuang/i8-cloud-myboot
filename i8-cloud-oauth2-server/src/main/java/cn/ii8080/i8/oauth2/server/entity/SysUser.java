package cn.ii8080.i8.oauth2.server.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.google.common.collect.Lists;
import lombok.Data;
import org.springframework.beans.BeanUtils;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * @author Liu Dong Cai
 * @since 2020-05-05
 */
@Data
@TableName("sys_user")
public class SysUser extends Model<SysUser> implements UserDetails {
    public SysUser() {
    }

    public SysUser(Object o) {
        BeanUtils.copyProperties(o, this);
    }

    @TableId(value = "id", type = IdType.ASSIGN_UUID)
    private String id;

    @Override
    protected Serializable pkVal() {
        return id;
    }

    @TableField("username")
    private String username;

    @TableField("password")
    private String password;

    @TableField("is_account_non_expired")
    private Boolean isAccountNonExpired;

    @TableField("is_account_non_locked")
    private Boolean isAccountNonLocked;

    @TableField("is_credentials_non_expired")
    private Boolean isCredentialsNonExpired;

    @TableField("is_enabled")
    private Boolean isEnabled;

    @TableField("nick_name")
    private String nickName;

    @TableField("mobile")
    private String mobile;

    @TableField("email")
    private String email;

    @TableField("create_date")
    private Date createDate;

    @TableField("update_date")
    private Date updateDate;

    @TableField(exist = false)
    private Collection<? extends GrantedAuthority> authorities;

    @Override
    public boolean isAccountNonExpired() {
        return this.isAccountNonExpired==null?false:this.isAccountNonExpired;
    }

    @Override
    public boolean isAccountNonLocked() {
        return this.isAccountNonLocked==null?false:this.isAccountNonLocked;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return this.isCredentialsNonExpired==null?false:this.isCredentialsNonExpired;
    }

    @Override
    public boolean isEnabled() {
        return this.isEnabled==null?false:this.isEnabled;
    }

    // 父接口认证方法 end

    /**
     * 拥有角色集合
     */
    @TableField(exist = false)
    private List<SysRole> roleList = Lists.newArrayList();
    /**
     * 获取所有角色id
     */
    @TableField(exist = false)
    private List<String> roleIds = Lists.newArrayList();
    public List<String> getRoleIds() {
        if(CollectionUtils.isNotEmpty(roleList)) {
            roleIds = Lists.newArrayList();
            for(SysRole role : roleList) {
                roleIds.add(role.getId());
            }
        }
        return roleIds;
    }

    @TableField(exist = false)
    private List<SysPermission> permissions = Lists.newArrayList();
}
