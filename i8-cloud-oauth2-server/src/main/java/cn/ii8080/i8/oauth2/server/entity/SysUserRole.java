package cn.ii8080.i8.oauth2.server.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import org.springframework.beans.BeanUtils;

import java.io.Serializable;

/**
 * @author Liu Dong Cai
 * @since 2020-05-05
 */
@Data
@TableName("sys_user_role")
public class SysUserRole extends Model<SysUserRole> {
    public SysUserRole() {
    }

    public SysUserRole(Object o) {
        BeanUtils.copyProperties(o, this);
    }

    @TableId(value = "id", type = IdType.ASSIGN_UUID)
    private String id;

    @Override
    protected Serializable pkVal() {
        return id;
    }

    @TableField("user_id")
    private String userId;

    @TableField("role_id")
    private String roleId;

}
