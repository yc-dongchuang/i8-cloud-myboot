package cn.ii8080.i8.oauth2.server.mapper;

import cn.ii8080.i8.oauth2.server.entity.SysPermission;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Flush;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author Liu Dong Cai
 * @since 2020-05-05
 */
@Mapper
public interface SysPermissionMapper extends BaseMapper<SysPermission> {
    List<SysPermission> getPermissionByUserId(@Param("userId") String userId);
}
