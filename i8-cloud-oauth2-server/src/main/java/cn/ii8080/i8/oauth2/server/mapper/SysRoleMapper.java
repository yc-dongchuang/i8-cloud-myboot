package cn.ii8080.i8.oauth2.server.mapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import cn.ii8080.i8.oauth2.server.entity.SysRole;
/**
 * 
 * @author Liu Dong Cai
 * @since 2020-05-05
 */
public interface SysRoleMapper extends BaseMapper<SysRole> {
//todo test
}
