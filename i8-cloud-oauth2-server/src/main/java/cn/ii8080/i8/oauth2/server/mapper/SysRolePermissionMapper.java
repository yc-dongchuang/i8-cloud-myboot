package cn.ii8080.i8.oauth2.server.mapper;

import cn.ii8080.i8.oauth2.server.entity.SysRolePermission;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @author Liu Dong Cai
 * @since 2020-05-05
 */
public interface SysRolePermissionMapper extends BaseMapper<SysRolePermission> {

}
