package cn.ii8080.i8.oauth2.server.mapper;

import cn.ii8080.i8.oauth2.server.entity.SysPermission;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import cn.ii8080.i8.oauth2.server.entity.SysUser;

/**
 * @author Liu Dong Cai
 * @since 2020-05-05
 */
public interface SysUserMapper extends BaseMapper<SysUser> {

}
