package cn.ii8080.i8.oauth2.server.service;

import cn.ii8080.i8.oauth2.server.entity.SysUserRole;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Liu Dong Cai
 * @since 2020-05-05
 */
public interface ISysUserRoleService extends IService<SysUserRole> {

}
