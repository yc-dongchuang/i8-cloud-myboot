package cn.ii8080.i8.oauth2.server.service.impl;

import cn.ii8080.i8.oauth2.server.entity.SysPermission;
import cn.ii8080.i8.oauth2.server.mapper.SysPermissionMapper;
import cn.ii8080.i8.oauth2.server.service.ISysPermissionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author Liu Dong Cai
 * @since 2020-05-05
 */
@Service
public class SysPermissionServiceImpl extends ServiceImpl<SysPermissionMapper, SysPermission> implements ISysPermissionService {

    @Override
    public List<SysPermission> getGrantedAuthorityByUsername(String userId) {
        return this.getBaseMapper().getPermissionByUserId(userId);
    }
}
