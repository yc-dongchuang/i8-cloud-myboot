package cn.ii8080.i8.oauth2.server.service.impl;

import cn.ii8080.i8.oauth2.server.entity.SysRolePermission;
import cn.ii8080.i8.oauth2.server.mapper.SysRolePermissionMapper;
import cn.ii8080.i8.oauth2.server.service.ISysRolePermissionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author Liu Dong Cai
 * @since 2020-05-05
 */
@Service
public class SysRolePermissionServiceImpl extends ServiceImpl<SysRolePermissionMapper, SysRolePermission> implements ISysRolePermissionService {

}
