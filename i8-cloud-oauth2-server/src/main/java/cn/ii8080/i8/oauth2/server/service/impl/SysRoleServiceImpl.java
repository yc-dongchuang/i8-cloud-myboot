package cn.ii8080.i8.oauth2.server.service.impl;

import cn.ii8080.i8.oauth2.server.entity.SysRole;
import cn.ii8080.i8.oauth2.server.mapper.SysRoleMapper;
import cn.ii8080.i8.oauth2.server.service.ISysRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author Liu Dong Cai
 * @since 2020-05-05
 */
@Service
public class SysRoleServiceImpl extends ServiceImpl<SysRoleMapper, SysRole> implements ISysRoleService {

}
